/**
 * ./src/ index.js
 */
import React, { Component } from "react";
import ReactDOM from "react-dom";

import FormGenerator from "./components/form-generator";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {
        return (
            <div>
                <FormGenerator />
            </div>
        );
    }
}

ReactDOM.render(
    < App / >
,   document.querySelector('.container')
);

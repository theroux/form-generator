import React, { Component } from "react";
import RenderList from "./render-list";
import TypeSelector from "./htmlComponents/type-selector";
import FormCreator from "./form-creator";

class FormGenerator extends Component {
    constructor(props) {
        super(props);

        this.state = {
            availableTypes: [
                {
                    type: "label"
                },
                {
                    type: "input"
                },
                {
                    type: "checkbox"
                },
            ],
            formsData: [],
            elements: [],
            isFormPresent: false,
            generateProcess: false
        };
    }

    newElement(type){
        this.setState({
            elements: this.state.elements.concat({type: type, value: ""})
        });
    }

    changeItemValue(value, index) {
        this.state.elements[index].value = value;
        this.setState({
            elements: this.state.elements
        });
    }

    saveJsonFile(text) {
        var a = document.getElementById("hiddenLink");
        this.state.elements.push({type: "button", value: "Submit"});
        var json = JSON.stringify(this.state.elements);
        var file = new Blob([json], {type: "text/plain"});
        a.href = URL.createObjectURL(file);
        a.download = "result.json";
        this.setState({
            elements: [],
            generateProcess: false
        });
    }

    fileReaderHandler(input) {
        var fr = new FileReader();
        fr.onload = (event) => {
            var obj = JSON.parse(event.target.result);
            this.setState({
                formsData: this.state.formsData.concat([obj])
            });
            input.value = "";
        };
        fr.readAsText(input.files[0]);
    }

    generateNewForm(){
        this.setState({
            generateProcess: true
        })
    }

    createTestForm(){
        var testForm = [{
                type: "label",
                value: "Login:"
            },
            {
                type: "input",
                value: "Enter your login"
            },
            {
                type: "label",
                value: "Password:"
            },
            {
                type: "input",
                value: "Enter your password"
            },
            {
                type: "button",
                value: "Submit"
            }];
        this.setState({
            formsData: this.state.formsData.concat([testForm])
        });
    }

    render() {
        if (this.state.generateProcess) {
            return (
                <section className="new-form-section">
                    <TypeSelector
                        list={this.state.availableTypes}
                        createElement={this.newElement.bind(this)}
                    />
                    <RenderList
                        list={this.state.elements}
                        changeItemValue={this.changeItemValue.bind(this)}
                        saveJsonFile={this.saveJsonFile.bind(this)}
                    />
                </section>
            )
        }
        return (
            <div>
                <section className="form-generation-section">
                    <button
                        className="btn form-generation-section__test"
                        onClick={this.createTestForm.bind(this)}
                    >
                        Create Example Form
                    </button>
                    <button
                        className="btn form-generation-section__new"
                        onClick={this.generateNewForm.bind(this)}
                    >
                        Generate New Form
                    </button>
                </section>
                <section className="form-creator-section">
                    <h2 className="form-creator-section__header">Append form from your JSON file: </h2>
                    <div className="file-upload">
                        +
                        <input
                            type="file"
                            onChange={ event => this.fileReaderHandler(event.target) }
                        />
                    </div>
                </section>
                <FormCreator
                    forms={this.state.formsData}
                />
            </div>
        );
    };
}
export default FormGenerator;

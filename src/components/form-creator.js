import React from "react";
import FormItem from "./htmlComponents/formItem";

const FormCreator = (props) => {
    const form = props.forms.map( (item, index) => {
       return (
           <FormItem
               key={index}
               fields={item}
           />
       )
    });
    return (
        <div>
            {form}
        </div>
    )
};

export default FormCreator;
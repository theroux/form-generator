import React from "react";


const Label = ({text}) => {
    return (
        <div>
            <label className="form-item__label">{text}</label>
        </div>
    )
};

export default Label;
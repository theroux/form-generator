import React, { Component } from "react";

class TypeSelector extends Component {
    constructor(props){
        super(props);
        this.state = {
            selected: ""
        }
    }
    appendElement() {
        if (this.state.selected){
            this.props.createElement(this.state.selected);
        }
    }
    setSelectedState(value){
        this.setState(
            {selected: value}
        );
    }
    render(){
        const item = this.props.list.map( (item, index) => {
            return (
                <option
                    key={index}
                    value={item.type}
                >
                    {item.type}
                </option>
            )
        });
        return(
            <section className="selection">
                <select
                    onChange={event => this.setSelectedState(event.target.value)}
                >
                    <option
                        default
                        hidden
                    >
                        Choose option
                    </option>
                    {item}
                </select>
                <button
                    className="btn"
                    onClick={this.appendElement.bind(this)}
                >
                    Add
                </button>
            </section>
        )
    }
}

export default TypeSelector;
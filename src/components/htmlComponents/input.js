import React from "react";


const Input = ({text}) => {
    return (
        <div>
            <input
                className="form-item__input"
                placeholder={text}
            />
        </div>
    )
};

export default Input;
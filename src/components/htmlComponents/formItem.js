import React from "react";
import Label from "./label";
import Input from "./input";
import Checkbox from "./checkbox";

const FormItem = (props) => {
    const formItem = props.fields.map( (item, index) => {
        if (item.type == "label") {
            return (
                <Label
                    key={index}
                    text={item.value}
                />
            )
        } else if (item.type == "input") {
            return (
                <Input
                    key={index}
                    text={item.value}
                />
            )
        } else if (item.type == "checkbox"){
            return (
                <Checkbox
                    key={index}
                    text={item.value}
                />
            )
        } else if (item.type == "button"){
            return (
                <button
                    key={index}
                    type="submit"
                    className="btn submit-btn"
                >
                    {item.value}
                </button>
            )
        }
    });
    return (
        <form className="form-item">
            {formItem}
        </form>
    )
};

export default FormItem;
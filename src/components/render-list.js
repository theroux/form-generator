import React, { Component } from "react";

class RenderList extends Component {
    constructor(props) {
        super(props);
    }
    update (value, index) {
        this.props.changeItemValue(value, index);
    }
    render (){
        const item = this.props.list.map( (item, index) => {
                return (
                    <div
                        key={index}>
                        <label className="form-item__label">{item.type} text: </label>
                        <input onBlur={event => this.update(event.target.value, index)} placeholder=""/>
                    </div>
                );
        });
        if (item.length > 0){
            return (
                <div className="new-fields">
                    <div className="new-fields__item">{item}</div>
                    <a
                        id="hiddenLink"
                        onClick={this.props.saveJsonFile}
                        className="btn"
                    >
                        Save
                    </a>
                </div>
            )
        } else {
            return (
                <div></div>
            )
        }
    };
}
export default RenderList;